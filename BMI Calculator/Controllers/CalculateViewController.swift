//
//  ViewController.swift
//  BMI Calculator

import UIKit

class CalculateViewController: UIViewController {
    
    var calculatorBrain = CalculatorBrain() // calling the struct

    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightSlider: UISlider!
    @IBOutlet weak var weightSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func heightSliderChanged(_ sender: UISlider) {
        //  "%.2f" : it shows just decimals/float after point(.),  sender.value = slider current value
        heightLabel.text = "\( String(format: "%.2f", sender.value))m"
    }
    
    @IBAction func weightSliderChanged(_ sender: UISlider) {
        let weight = String(format: "%.0f", sender.value)
        weightLabel.text = "\(weight)Kg"
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        let height = heightSlider.value
        let weight = weightSlider.value

        calculatorBrain.calculateBMI(height: height, weight: weight)
        
        performSegue(withIdentifier: "goToResult", sender: self) // invoque when the user make touch on "calcualte button" the segue/link to proceed to show ¨resultview¨ page/controller, this will be listen in " override func prepare" to pass parameters to another page
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // prepare is a heritage from UIViewController:  listen when we want to go back to this page from resultview page/controller
        if segue.identifier == "goToResult" {
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.bmiValue = calculatorBrain.getBMIValue()
            destinationVC.advice = calculatorBrain.getAdvice()
            destinationVC.color = calculatorBrain.getColor()
        }
    }
}























